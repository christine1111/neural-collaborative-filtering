import pandas as pd
import numpy as np

input_path = 'ml-1m/ratings.dat'
train_output_path = 'data/ml-1m.train.rating'
test_output_path = 'data/ml-1m.test.rating'


def get_train_test_df(interactions):
    print("Size of the entire dataset:{}".format(interactions.shape))
    interactions.sort_values(by=['timestamp'], inplace=True)
    last_interaction_mask = interactions.duplicated(subset={'userID'}, keep="last")

    # the latest of each user-item interaction is removed from the test set
    train_df = interactions[last_interaction_mask]
    test_df = interactions[~last_interaction_mask]

    train_df.sort_values(by=["userID", 'timestamp'], inplace=True)
    test_df.sort_values(by=["userID", 'timestamp'], inplace=True)
    return train_df, test_df


def report_stats(interactions, train_df, test_df):
    whole_size = interactions.shape[0] * 1.0
    train_size = train_df.shape[0]
    test_size = test_df.shape[0]
    print("Total No. of Records = {}".format(whole_size))
    print("Train size = {}, Test size = {}".format(train_size, test_size))
    print("Train % = {}, Test % ={}".format(train_size / whole_size, test_size / whole_size))


def main():
    interactions = pd.read_csv(input_path, sep="::", names=['userID', 'movieID', 'rating', 'timestamp'],
                               engine='python')
    interactions['rating'] = 1
    train_df, test_df = get_train_test_df(interactions)
    train_df.to_csv(train_output_path, header=False, index=False, sep='\t')
    test_df.to_csv(test_output_path, header=False, index=False, sep='\t')
    report_stats(interactions, train_df, test_df)
    return 0


if __name__ == "__main__":
    main()

import numpy as np
from keras import initializers
from keras.models import Model
from keras.layers import Embedding, Dropout,Input, Dense, Flatten
from keras.optimizers import Adam
from keras.regularizers import l2

from Dataset import Dataset
from Evaluate import evaluate_model
from time import time
from keras.layers import multiply


def get_model(num_users, num_items, latent_dim, regs=[0,0]):
    # Input variables
    user_input = Input(shape=(1,), dtype='int32', name='user_input')
    item_input = Input(shape=(1,), dtype='int32', name='item_input')

    MF_Embedding_User = Embedding(input_dim=num_users, output_dim=latent_dim,
                                  embeddings_initializer=initializers.random_normal(),
                                  embeddings_regularizer=l2(regs[0]), input_length=1, name='user_embedding')
   
    MF_Embedding_Item = Embedding(input_dim=num_items, output_dim=latent_dim,
                                  embeddings_initializer=initializers.random_normal(),
                                  embeddings_regularizer=l2(regs[1]),
                                  input_length=1, name='item_embedding')
    
    # Crucial to flatten an embedding vector!
    user_latent = Flatten()(MF_Embedding_User(user_input))
    item_latent = Flatten()(MF_Embedding_Item(item_input))
    
    # Element-wise product of user and item embeddings 
    predict_vector = multiply([user_latent, item_latent])
    
    
    # Final prediction layer
    prediction = Dense(1, activation='sigmoid',kernel_initializer=initializers.lecun_normal(), name='prediction')(predict_vector)
    model = Model(inputs=[user_input, item_input], output=prediction)

    return model


def get_train_instances(train, num_negatives):
    user_input, item_input, labels = [], [], []
    num_users = train.shape[0]
    for (u, i) in train.keys():
        # positive instance
        user_input.append(u)
        item_input.append(i)
        labels.append(1)
        # negative instances
        for t in range(num_negatives):
            j = np.random.randint(num_items)
            # while train.has_key((u, j)):
            while (u, j) in train.keys():
                j = np.random.randint(num_items)
            user_input.append(u)
            item_input.append(j)
            labels.append(0)
    return user_input, item_input, labels


if __name__ == '__main__':

	num_epochs = 30
	num_factors = 64
	batch_size = 2**17
	regs = eval('[0, 0]')
	num_negatives = 4
	learning_rate = 0.0045
	b1 = 0.25
	b2 = 0.5
	eps = 1e-8
	verbose = 1
	model_out = 1

	hr_list = []
	ndcg_list = []
	bce_list = []
	epch_list = []

	topK = 10
	evaluation_threads = 1  # mp.cpu_count()
	model_alias = 'GMF_%d.h5' %(time())
	model_out_file = 'Pretrain/GMF_%d.h5' % (time())

	# Loading data
	t1 = time()

	dataset = Dataset('data/' + 'ml-1m')
	train, testRatings, testNegatives = dataset.trainMatrix, dataset.testRatings, dataset.testNegatives
	num_users, num_items = train.shape
	print("Load data done [%.1f s]. #user=%d, #item=%d, #train=%d, #test=%d" 
		% (time()-t1, num_users, num_items, train.nnz, len(testRatings)))

	# Build model
	model = get_model(num_users, num_items, num_factors, regs) 
	model.compile(optimizer=Adam(lr=learning_rate, beta_1=b1, beta_2=b2, epsilon=eps), loss='binary_crossentropy')

	# Init performance
	t1 = time()
	(hits, ndcgs) = evaluate_model(model, testRatings, testNegatives, topK, evaluation_threads)
	hr, ndcg = np.array(hits).mean(), np.array(ndcgs).mean()
	print('Init: HR = %.4f, NDCG = %.4f\t [%.1f s]' % (hr, ndcg, time()-t1))

	# Train model
	best_hr, best_ndcg, best_iter = hr, ndcg, -1
	for epoch in range(num_epochs):
		t1 = time()
		# Generate training instances
		user_input, item_input, labels = get_train_instances(train, num_negatives)
		# Training
		hist = model.fit([np.array(user_input), np.array(item_input)], np.array(labels),batch_size=batch_size, epochs=1, verbose=0, shuffle=True)
		t2 = time()

		# Evaluation
		if epoch % verbose == 0:
			(hits, ndcgs) = evaluate_model(model, testRatings, testNegatives, topK, evaluation_threads)
			hr, ndcg, loss = np.array(hits).mean(), np.array(ndcgs).mean(), hist.history['loss'][0]

			hr_list.append(hr)
			ndcg_list.append(ndcg)
			bce_list.append(loss)
			epch_list.append(epoch)

			print('Iteration %d [%.1f s]: HR = %.4f, NDCG = %.4f, loss = %.4f [%.1f s]' % (epoch,  t2-t1, hr, ndcg, loss, time()-t2))
			if hr > best_hr:
				best_hr, best_ndcg, best_iter = hr, ndcg, epoch
				if model_out > 0:
					model.save_weights(model_out_file, overwrite=True)

	print("End. Best Iteration %d:  HR = %.4f, NDCG = %.4f. " %(best_iter, best_hr, best_ndcg))
	if model_out > 0:
		print("The best GMF model is saved to %s" % model_out_file)

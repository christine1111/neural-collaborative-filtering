import numpy as np
import matplotlib.pyplot as plt

from keras import initializers
from keras.regularizers import l2
from keras.models import Model
from keras.layers import Embedding, Dropout, Input, Dense, BatchNormalization, merge, Flatten, concatenate, multiply
from keras.optimizers import Adam, SGD

from Evaluate import evaluate_model
from Dataset import Dataset
from time import time
import GMF, MLP


def get_model(num_users, num_items, mf_dim, layers, reg_layers, reg_mf, dropout_rate):
    assert len(layers) == len(reg_layers)
    num_layer = len(layers)  # Number of layers in the MLP

    # User and Item inputs
    user_input = Input(shape=(1,), dtype='int32', name='user_input')
    item_input = Input(shape=(1,), dtype='int32', name='item_input')

    # User and Item embeddings for MF 
    MF_Embedding_User = Embedding(input_dim=num_users, output_dim=mf_dim, name='mf_embedding_user',
                                  embeddings_initializer=initializers.random_normal(),
                                  embeddings_regularizer=l2(reg_mf),
                                  input_length=1)
    MF_Embedding_Item = Embedding(input_dim=num_items, output_dim=mf_dim, name='mf_embedding_item',
                                  embeddings_initializer=initializers.random_normal(),
                                  embeddings_regularizer=l2(reg_mf), input_length=1)

    # User and Item embeddings for MLP
    MLP_Embedding_User = Embedding(input_dim=num_users, output_dim=int(layers[0] / 2), name="mlp_embedding_user",
                                   embeddings_initializer=initializers.random_normal(),
                                   embeddings_regularizer=l2(reg_layers[0]), input_length=1)
    MLP_Embedding_Item = Embedding(input_dim=num_items, output_dim=int(layers[0] / 2), name='mlp_embedding_item',
                                   embeddings_initializer=initializers.random_normal(),
                                   embeddings_regularizer=l2(reg_layers[0]), input_length=1)

    # MF Layer
    mf_user_latent = Flatten()(MF_Embedding_User(user_input))
    mf_item_latent = Flatten()(MF_Embedding_Item(item_input))
    mf_vector = multiply([mf_user_latent, mf_item_latent])
    mf_vector = Dropout(rate=dropout_rate)(mf_vector)

    # MLP Layers
    mlp_user_latent = Flatten()(MLP_Embedding_User(user_input))
    mlp_item_latent = Flatten()(MLP_Embedding_Item(item_input))
    mlp_vector = concatenate([mlp_user_latent, mlp_item_latent])

    for idx in range(1, num_layer):
        layer = Dense(layers[idx], kernel_regularizer=l2(reg_layers[idx]), activation='relu', name="layer%d" % idx)
        mlp_vector = layer(mlp_vector)
        mlp_vector = Dropout(rate=dropout_rate)(mlp_vector)

    # Concatenate MF and MLP parts
    predict_vector = concatenate([mf_vector, mlp_vector])

    # Final prediction layer
    prediction = Dense(1, activation='sigmoid', kernel_initializer=initializers.lecun_normal(),
                       name="prediction")(predict_vector)

    model = Model(inputs=[user_input, item_input],
                  outputs=prediction)

    return model


def load_pretrain_model(model, gmf_model, mlp_model, num_layers):
    # User and Item embeddings from GMF
    gmf_user_embeddings = gmf_model.get_layer('user_embedding').get_weights()
    gmf_item_embeddings = gmf_model.get_layer('item_embedding').get_weights()
    model.get_layer('mf_embedding_user').set_weights(gmf_user_embeddings)
    model.get_layer('mf_embedding_item').set_weights(gmf_item_embeddings)

    # User and Item embeddings from MLP 
    mlp_user_embeddings = mlp_model.get_layer('user_embedding').get_weights()
    mlp_item_embeddings = mlp_model.get_layer('item_embedding').get_weights()
    model.get_layer('mlp_embedding_user').set_weights(mlp_user_embeddings)
    model.get_layer('mlp_embedding_item').set_weights(mlp_item_embeddings)

    # MLP layers
    for i in range(1, num_layers):
        mlp_layer_weights = mlp_model.get_layer('layer%d' % i).get_weights()
        model.get_layer('layer%d' % i).set_weights(mlp_layer_weights)

    gmf_prediction = gmf_model.get_layer('prediction').get_weights()
    mlp_prediction = mlp_model.get_layer('prediction').get_weights()
    new_weights = np.concatenate((gmf_prediction[0], mlp_prediction[0]), axis=0)
    new_b = gmf_prediction[1] + mlp_prediction[1]
    model.get_layer('prediction').set_weights([0.5 * new_weights, 0.5 * new_b])
    return model


# Function to generate training samples 
def get_train_instances(train, num_negatives):
    user_input, item_input, labels = [], [], []
    num_users = train.shape[0]
    for (u, i) in train.keys():
        # positive instance
        user_input.append(u)
        item_input.append(i)
        labels.append(1)
        # negative instances
        for t in range(num_negatives):
            j = np.random.randint(num_items)
            while (u, j) in train.keys():
                j = np.random.randint(num_items)
            user_input.append(u)
            item_input.append(j)
            labels.append(0)
    return user_input, item_input, labels


# Function to generate performance graph
def plot_statistics(epch_list, hr_list, ndcg_list, loss_list, model_alias, path):
    plt.rcParams["axes.grid"] = True  # enables gridlines
    plt.rcParams["axes.grid.axis"] = "y"  # enables only horizontal (y-axis) gridlines
    plt.rcParams["axes.axisbelow"] = True  # sets horizontal grid behind bars
    plt.rcParams['figure.figsize'] = (10, 10)  # plot size
    plt.rcParams.update({'figure.autolayout': True})
    plt.figure()
    epoch = np.array(epch_list)
    hr = np.array(hr_list)
    loss = np.array(loss_list)
    plt.tick_params(axis='x', labelsize=18)
    plt.tick_params(axis='y', labelsize=18)
    plt.plot(epoch, hr, linestyle='-', marker='D', markersize='10', color='b', label="HR@10")
    plt.plot(epoch, loss, linestyle='-', marker='s', markersize='10', color='r', label="Loss")
    plt.xlabel("Iteration", fontsize=18, fontweight='bold')
    plt.ylabel("Value", fontsize=18, fontweight='bold')
    plt.legend()
    plt.savefig("results/neumf_not_pretrained.png", dpi=300)
    return


if __name__ == '__main__':
    num_epochs = 0
    batch_size = 2 ** 17
    mf_dim = 64
    layers = eval('[1024,512,256,128,64]')
    reg_mf = 0
    reg_layers = eval('[0,0,0,0,0]')
    num_negatives = 4
    learning_rate = 0.0045
    b1 = 0.25
    b2 = 0.5
    eps = 1e-8
    verbose = 1
    mlp_pretrain = ''
    mf_pretrain = ''
    model_out = 1
    dropout_rate = 0.2

    hr_list = []
    ndcg_list = []
    bce_list = []
    epch_list = []

    evaluation_threads = 1
    topK = 10
    model_alias = 'NeuMF_%d.h5' % (time())
    model_out_file = 'Pretrain/NeuMF_%d.h5' % (time())

    # Loading data
    t1 = time()
    dataset = Dataset('data/' + 'ml-1m')
    train, testRatings, testNegatives = dataset.trainMatrix, dataset.testRatings, dataset.testNegatives
    num_users, num_items = train.shape
    print("Load data done [%.1f s]. #user=%d, #item=%d, #train=%d, #test=%d"
          % (time() - t1, num_users, num_items, train.nnz, len(testRatings)))

    # Build model
    model = get_model(num_users, num_items, mf_dim, layers, reg_layers, reg_mf, dropout_rate)
    model.compile(optimizer=Adam(lr=learning_rate, beta_1=b1, beta_2=b2, epsilon=eps), loss='binary_crossentropy')
    # model.compile(optimizer=SGD(lr=learning_rate), loss='binary_crossentropy')

    # Load pretrain model
    if mf_pretrain != '' and mlp_pretrain != '':
        gmf_model = GMF.get_model(num_users, num_items, mf_dim)
        gmf_model.load_weights(mf_pretrain)
        mlp_model = MLP.get_model(num_users, num_items, layers, reg_layers)
        mlp_model.load_weights(mlp_pretrain)
        model = load_pretrain_model(model, gmf_model, mlp_model, len(layers))
        print("Load pretrained GMF (%s) and MLP (%s) models done. " % (mf_pretrain, mlp_pretrain))

    # Init performance
    model.load_weights("Pretrain/NeuMF_1566160454.h5")
    print('Pretrain/NeuMF_1566160454.h5')
    import pdb;

    pdb.set_trace()
    (hits, ndcgs) = evaluate_model(model, testRatings, testNegatives, topK, evaluation_threads)
    hr, ndcg = np.array(hits).mean(), np.array(ndcgs).mean()
    print('Init: HR = %.4f, NDCG = %.4f' % (hr, ndcg))
    best_hr, best_ndcg, best_iter = hr, ndcg, -1
    if model_out:
        model.save_weights(model_out_file, overwrite=True)

        # Training model
    for epoch in range(num_epochs):
        t1 = time()
        # Generate training instances
        user_input, item_input, labels = get_train_instances(train, num_negatives)

        # Training
        hist = model.fit([np.array(user_input), np.array(item_input)],  # input
                         np.array(labels),  # labels
                         batch_size=batch_size, epochs=1, verbose=0, shuffle=True)
        t2 = time()

        # Evaluation
        if epoch % verbose == 0:
            (hits, ndcgs) = evaluate_model(model, testRatings, testNegatives, topK, evaluation_threads)
            hr, ndcg, loss = np.array(hits).mean(), np.array(ndcgs).mean(), hist.history['loss'][0]

            hr_list.append(hr)
            ndcg_list.append(ndcg)
            bce_list.append(loss)
            epch_list.append(epoch)

            print('Iteration %d [%.1f s]: HR = %.4f, NDCG = %.4f, loss = %.4f [%.1f s]'
                  % (epoch, t2 - t1, hr, ndcg, loss, time() - t2))
            if hr > best_hr:
                best_hr, best_ndcg, best_iter = hr, ndcg, epoch
                if model_out > 0:
                    model.save_weights(model_out_file, overwrite=True)

    print("End. Best Iteration %d:  HR = %.4f, NDCG = %.4f. " % (best_iter, best_hr, best_ndcg))
    if model_out > 0:
        print("The best NeuMF model is saved to %s" % model_out_file)

    plot_statistics(epch_list, hr_list, ndcg_list, bce_list, model_alias, "figs/")

# Movie Recommendations Using Deep Learning.

Neural Collaborative Filtering using the work of Xiangnan He, Lizi Liao, Hanwang Zhang, Liqiang Nie, Xia Hu and Tat-Seng Chua in the [Neural Collaborative Filtering paper](https://www.comp.nus.edu.sg/~xiangnan/papers/ncf.pdf). All of the code in this repository was modified from the author's code which can be found [here](https://github.com/hexiangnan/neural_collaborative_filtering).

### Prerequisites

Keras was used with TensorFlow as the backend. 

Keras version 2.2.4 <br />
TensorFlow 1.13.1

### Installing

First navigate into the project folder

```
cd Neural-Collaborative-Filtering
```

Install dependencies: 

```
pip install -r requirements.txt
```

## Running the code

To generate the train-test split run the following:

```
python Data_split.py
```

To run the NeuMF script:

```
python NeuMF.py
```

